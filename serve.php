<?php

if(!is_file(dirname($_SERVER["argv"][0])."\composer.json")) {
    chdir(dirname($_SERVER["argv"][0]));
    exec('composer init --name lidtlakrids/lakridscli --description lakrids_cli --require phpseclib/phpseclib:~3.0 -n');
    exec('composer config -g -- disable-tls false');
    exec('composer install');
}
require_once(dirname($_SERVER["argv"][0]).'\vendor\autoload.php');

class Serve {
    private $key                = null;
    private $project_name       = null;
    private $project_config     = [];

    /**
     * Build constructor.
     * Setting up the environment to the private setters, and starts the script
     */
    public function __construct() {

        try {
            $this->init();
            $this->serve();
        } catch(\Exception $e) {
            // Every throw will be handled here, which makes this work like a global error handler
            fwrite(STDERR, $e->getMessage());
            exit(0x0a);
        }
    }

    /**
     * Simple initial of script before running
     * Creating and setting the needed structure for the system to be able to work
     */
    private function init() {
        if(!is_file(CLI_DIR."\private.key")) {
            file_put_contents(CLI_DIR."\private.key", $this->generateHash());
        }

        if(!is_dir(PROJECTS_DIR)) {
            mkdir(PROJECTS_DIR);
        }

        $this->key = file_get_contents(CLI_DIR."\private.key");
    }

    /**
     * The actual build script
     * Will deploy a docker setup from current working dir
     */
    public function serve() {
        // If there is no wp-content, stop
        if(!is_dir(CALLER_DIR."\wp-content")) {
            Helper::log("ERROR: Cannot find wp-content folder in working directory");
            exit;
        }

        $this->project_name = basename(CALLER_DIR);
        if(!is_dir(PROJECTS_DIR."\\$this->project_name")) {
            if(!is_dir(PROJECTS_DIR."\\$this->project_name")) {
                mkdir(PROJECTS_DIR."\\$this->project_name");
            }
            if(!is_file(PROJECTS_DIR."\\$this->project_name\dump.sql")) {
                touch(PROJECTS_DIR."\\$this->project_name\dump.sql");
            }
            if(!is_dir(PROJECTS_DIR."\\$this->project_name\mysql")) {
                mkdir(PROJECTS_DIR."\\$this->project_name\mysql");
            }
            if(!is_dir(PROJECTS_DIR."\\$this->project_name\site")) {
                mkdir(PROJECTS_DIR."\\$this->project_name\site");
            }
        }



        $this->setDockerCompose();
        Helper::log("Starting the local lakrids-development setup");
        Helper::log("Please wait while everything is being setup...");
        chdir(PROJECTS_DIR."/".$this->project_name);
        exec('docker-compose up -d');
        Helper::log("Dev site ready:", 2);
        Helper::log("Dev URL:    http://localhost:3000");
        Helper::log("Wordpress:  http://localhost:8080");
        Helper::log("PHPMyAdmin: http://localhost:3050", 2);
        Helper::log("It can take several minutes for the database to be ready, please wait for this.");
        chdir(CALLER_DIR."/wp-content/themes/lakrids");
        passthru('npm install');
//        exec('start http://localhost:3000');
        passthru('npm run lakrids');
    }

	private function setDockerCompose() {
		$data = '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcefghijklmnopqrstuvwxyz';
        $lidtLakridsPassword = substr(str_shuffle($data), 0, 12);
		ob_start();
		?>
version: "3"
services:
    db:
        image: mysql:5.7
        container_name: "<?= $this->project_name ?>_mysql"
        volumes:
            - '<?= PROJECTS_DIR ?>/<?= $this->project_name ?>/mysql:/var/lib/mysql'
            - '<?= PROJECTS_DIR ?>/<?= $this->project_name ?>/dump.sql:/docker-entrypoint-initdb.d/dump.sql'
        ports:
            - 3051:3306
        environment:
            MYSQL_USER: lidtlakrids
            MYSQL_DATABASE: wp_lakrids_local
            MYSQL_PASSWORD: lidtlakrids
            MYSQL_ROOT_PASSWORD: root
        restart: always
    phpmyadmin:
        container_name: <?= $this->project_name ?>_phpmyadmin
        depends_on:
            - db
        image: phpmyadmin/phpmyadmin
        ports:
            - 3050:80
        environment:
            PMA_HOST: db
            PMA_USER: lidtlakrids
            PMA_PASSWORD: lidtlakrids
        restart: always
    web:
        container_name: <?= $this->project_name ?>_wordpress
        depends_on:
            - db
        image: registry.gitlab.com/lidt-lakrids/lakrids-base-theme:wp
        ports:
            - 8080:80
        volumes:
            - '<?= CALLER_DIR ?>:/var/www/html'
            - '<?= CLI_DIR ?>/uploads.ini:/usr/local/etc/php/conf.d/uploads.ini'
        environment:
            WORDPRESS_DB_NAME: wp_lakrids_local
            WORDPRESS_DB_HOST: db
            WORDPRESS_DB_USER: lidtlakrids
            WORDPRESS_DB_PASSWORD: lidtlakrids
            WORDPRESS_CONFIG_EXTRA: |
                define("WP_HOME", "http://localhost:8080/");
                define("WP_SITEURL", "http://localhost:8080/");
                define("WP_MEMORY_LIMIT", "512M");
                define("WP_DOCKER", true);
                define("DISABLE_WP_CRON", true);
        restart: always
    wpcli:
        image: wordpress:cli
        volumes:
            - '<?= CALLER_DIR ?>:/var/www/html'
        depends_on:
            - db
            - web
        environment:
            WORDPRESS_DB_NAME: wp_lakrids_local
            WORDPRESS_DB_HOST: db
            WORDPRESS_DB_USER: lidtlakrids
            WORDPRESS_DB_PASSWORD: lidtlakrids
        command: >
            sh -c 'wp core install --path="/var/www/html" --url="http://localhost:8080" --title="LakridsBase" --admin_user=lidtlakrids --admin_password=<?= $lidtLakridsPassword ?> --admin_email=kontakt@lidtlakrids.dk --skip-email &&
            wp option update permalink_structure '/%postname%' &&
            wp plugin activate LakridsImage &&
            wp plugin activate LakridsCloud &&
            wp plugin activate advanced-custom-fields-pro &&
            wp plugin activate blockstudio &&
            wp theme activate lakrids'<?php
		$dockerComposerScript = ob_get_clean();
		file_put_contents(PROJECTS_DIR."\\$this->project_name\docker-compose.yml", $dockerComposerScript);
	}


    /**
     * Generates a random string, which is then hashed, to serve as a master key of this machine when saving credentials in files
     * @return string
     * @throws Exception
     */
    private function generateHash() {
        return bin2hex(random_bytes(22));
    }




}

// Run that mf
new Serve();