<?php

	class Remove {
		private $project_name       = null;

		/**
		 * Build constructor.
		 * Setting up the environment to the private setters, and starts the script
		 */
		public function __construct() {

			try {
				$this->remove();
			} catch(\Exception $e) {
				// Every throw will be handled here, which makes this work like a global error handler
				fwrite(STDERR, $e->getMessage());
				exit(0x0a);
			}
		}
		private function removeFolder($folderName) {
			if (is_dir($folderName))
				$folderHandle = opendir($folderName);
			if (!$folderHandle)
				return false;
			while($file = readdir($folderHandle)) {
				if ($file != "." && $file != "..") {
					if (!is_dir($folderName."/".$file))
						unlink($folderName."/".$file);
					else
						$this->removeFolder($folderName.'/'.$file);
				}
			}
			closedir($folderHandle);
			rmdir($folderName);
			return true;
		}

		/**
		 * The actual build script
		 * Will deploy a docker setup from current working dir
		 */
		public function remove() {
			$this->project_name = basename(CALLER_DIR);
			if(is_dir(PROJECTS_DIR."/".$this->project_name)) {
				$this->removeFolder(PROJECTS_DIR."/".$this->project_name);
			}

			if(is_dir(CALLER_DIR."/wp-admin")) {
				$this->removeFolder(CALLER_DIR."/wp-admin");
			}

			if(is_dir(CALLER_DIR."/wp-includes")) {
				$this->removeFolder(CALLER_DIR."/wp-includes");
			}

			if(is_file(CALLER_DIR."/wp-config.php")) {
				unlink(CALLER_DIR."/wp-config.php");
			}

			if(is_file(CALLER_DIR."/index.php")) {
				unlink(CALLER_DIR."/index.php");
			}

			if(is_file(CALLER_DIR."/license.txt")) {
				unlink(CALLER_DIR."/license.txt");
			}

			if(is_file(CALLER_DIR."/.htaccess")) {
				unlink(CALLER_DIR."/.htaccess");
			}

			if(is_file(CALLER_DIR."/readme.html")) {
				unlink(CALLER_DIR."/readme.html");
			}

			if(is_file(CALLER_DIR."/xmlrpc.php")) {
				unlink(CALLER_DIR."/xmlrpc.php");
			}

			if(is_file(CALLER_DIR."/wp-content/plugins/hello.php")) {
				unlink(CALLER_DIR."/wp-content/plugins/hello.php");
			}

			if(is_file(CALLER_DIR."/wp-content/themes/index.php")) {
				unlink(CALLER_DIR."/wp-content/themes/index.php");
			}

			if(is_file(CALLER_DIR."/wp-content/plugins/index.php")) {
				unlink(CALLER_DIR."/wp-content/plugins/index.php");
			}

			if(is_dir(CALLER_DIR."/wp-content/plugins/akismet")) {
				$this->removeFolder(CALLER_DIR."/wp-content/plugins/akismet");
			}

			$wpFiles = glob(CALLER_DIR."/wp-*.php");
			foreach($wpFiles as $file) {
				unlink($file);
			}

			$themes = glob(CALLER_DIR."/wp-content/themes/twenty*");
			foreach($themes as $theme) {
				$this->removeFolder($theme);
			}

			Helper::log("Project has been removed and cleared.");
		}
	}

	new Remove();