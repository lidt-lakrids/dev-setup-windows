<?php

class Init {
    public function __construct()
    {
        $this->checkOverwritesBeforeRunning();
        $this->importDefaultLakridsTheme();
    }


    private function checkOverwritesBeforeRunning()
    {
        if(is_dir(CALLER_DIR."/wp-content")) {
            $overwrite = Helper::user_input("It seems wordpress is already installed. Running this might overwrite your current files, do you want to continue? [y/n] ", ["y", "n"]);
            if($overwrite === "n") {
                exit;
            }
        }
    }

    private function importDefaultLakridsTheme()
    {
        chdir(CALLER_DIR);
        exec("git clone https://gitlab.com/lidt-lakrids/lakrids-base-theme.git ./temp");
        $dir = CALLER_DIR."/temp";
        $dirNew = CALLER_DIR;
        // Open a known directory, and proceed to read its contents
        if (is_dir($dir)) {
            if ($dh = opendir($dir)) {
                while (($file = readdir($dh)) !== false) {
                    // Fixed
                    if($file === "." || $file === ".." || $file === ".git") {
                        continue;
                    }
                    rename($dir.'/'.$file,$dirNew.'/'.$file);
                }
                closedir($dh);
            }
        }
        exec("rmdir .\\temp /s /q");
//        exec("rmdir .\.git /s /q");
    }


}

new Init();