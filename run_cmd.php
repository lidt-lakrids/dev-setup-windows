<?php

/**
 * Class Run CMD
 *
 * If you look at this, you are curious.
 * I like that <3
 *
 */
require_once(__DIR__."/Helper.php");
class Run extends Helper {


    public function __construct() {
        $args                   = $_SERVER["argv"];
        define("CLI_DIR",       dirname($args[0]));
        define("CALLER_DIR",    $args[1]);
        define("PROJECTS_DIR",  CLI_DIR."\\projects");
        define("ARGUMENTS",     array_slice($args, 2));

        try {
            $this->run();
        } catch(\Exception $e) {
            // Every throw will be handled here, which makes this work like a global error handler
            fwrite(STDERR, $e->getMessage());
            exit(0x0a);
        }
    }

    private function run() {
        if(empty(ARGUMENTS)) {
            self::help();
            exit;
        }

        $method = ARGUMENTS[0];
        if(!is_file(CLI_DIR."/".$method.".php")) {
            self::help();
        } else {
            // Files will be self running
            require_once(CLI_DIR."/".$method.".php");
        }
    }

}

new Run();

