<?php

class Pull {

    private $key                = null;
    private $projects_dir       = null;
    private $project_name       = null;
    private $project_config     = [];

    public function __construct() {
        $this->pull();
    }

    public function pull() {
        if(!is_file(CLI_DIR."\private.key")) {
            file_put_contents(CLI_DIR."\private.key", $this->generateHash());
        }

        if(!is_dir(PROJECTS_DIR)) {
            mkdir(PROJECTS_DIR);
        }

        $this->key = file_get_contents(CLI_DIR."\private.key");

        if(!is_dir(CALLER_DIR."\wp-content")) {
            Helper::log("ERROR: Cannot find wp-content folder in working directory");
            exit;
        }

        $this->project_name = basename(CALLER_DIR);
        if(!is_dir(PROJECTS_DIR."\\$this->project_name")) {
            if(!is_dir(PROJECTS_DIR."\\$this->project_name")) {
                mkdir(PROJECTS_DIR."\\$this->project_name");
            }
            if(!is_file(PROJECTS_DIR."\\$this->project_name\dump.sql")) {
                touch(PROJECTS_DIR."\\$this->project_name\dump.sql");
            }
            if(!is_dir(PROJECTS_DIR."\\$this->project_name\mysql")) {
                mkdir(PROJECTS_DIR."\\$this->project_name\mysql");
            }
            if(!is_dir(PROJECTS_DIR."\\$this->project_name\site")) {
                mkdir(PROJECTS_DIR."\\$this->project_name\site");
            }
        }

        if(!is_file(PROJECTS_DIR."\\$this->project_name\config.key")) {
            $this->createNewProject();
        }

        if(is_file(PROJECTS_DIR."\\$this->project_name\config.key")) {
            if(in_array("-reset", ARGUMENTS)) {
                $this->createNewProject();
            }
            $project_encrypted_configuration    = file_get_contents(PROJECTS_DIR."\\$this->project_name\config.key");
            $this->project_config               = json_decode(openssl_decrypt($project_encrypted_configuration, 'AES-192-CBC', $this->key), true);

            // Check if directory is empty
            $downloadProject = Helper::user_input("Do you want to download / update the project from remote now? [y/n]", ["y","n"]);
            if($downloadProject === "y") {
                Helper::log("Downloading / updating project. Please wait...");
                $this->testConnection();
                $this->downloadProject();
            }
        }
    }

    private function ftp_download_all_files($connection, $_from = null, $_to = null, $_from_dir = null) {
        if (isset($_from)) {
            if (!ftp_chdir($connection, $_from)) die("Dir on FTP not found: $_from");
            if (isset($_to)) {
                if (!is_dir($_to)) @mkdir($_to);
                if (!chdir($_to)) die("Dir on local not exists? $_to");
            }
        }

        $contents = ftp_mlsd($connection, '.');

        foreach ($contents as $p) {

            if ($p['type'] != 'dir' && $p['type'] != 'file') continue;

            $file = $p['name'];

            Helper::log(ftp_pwd($connection).'/'.$file);

            if (file_exists($file) && !is_dir($file) && filemtime($file) >= strtotime($p['modify'])) {
                Helper::log("File already exists and is not modified, skipping...");
            }
            elseif ($p['type'] == 'file' && @ftp_get($connection, $file, $file, FTP_BINARY)) {
                Helper::log("Downloaded...");
            }
            elseif ($p['type'] == 'dir' && @ftp_chdir($connection, $file)) {
                Helper::log("DIR IS: $file");
                Helper::log("COMMING FROM: $_from_dir");
                if($file === "minit" && $_from_dir === "uploads") {
                    Helper::log("Skipping minit folder...");
                    ftp_chdir($connection, '..');
                    continue;
                }
                if (strpos($file, 'twentytwenty') === 0) {
                    Helper::log("Skipping twentytwenty theme...");
                    ftp_chdir($connection, '..');
                    continue;
                }
                if ($file === "advancedassets_images") {
                    Helper::log("Skipping advanced assets generated images...");
                    ftp_chdir($connection, '..');
                    continue;
                }
                if($file === "themes") {
                    Helper::log("Skipping themes folder...");
                    ftp_chdir($connection, '..');
                    continue;
                }
                Helper::log("Dir changed to $file");
                if (!is_dir($file)) mkdir($file);
                chdir($file);
                $this->ftp_download_all_files($connection, null, null, $file);
                ftp_chdir($connection, '..');
                chdir('..');
            }
        }
    }

    private function downloadProject() {
        if($this->project_config["connection"]["type"] === "ftp") {
            if(!in_array("-sqlonly", ARGUMENTS)) {
                $this->downloadProjectFromFtp();
            }
            if(!in_array("-filesonly", ARGUMENTS)) {
                $this->downloadSqlDump();
            }

        }
        if($this->project_config["connection"]["type"] === "kinsta") {
            if(!in_array("-sqlonly", ARGUMENTS)) {
                $this->downloadProjectFromKinsta();
            }
            if(!in_array("-filesonly", ARGUMENTS)) {
                $this->downloadSqlFromKinsta();
            }
        }
    }

    private function downloadSqlFromKinsta() {
        $ssh = new \phpseclib3\Net\SSH2($this->project_config["connection"]["host"], $this->project_config["connection"]["port"]);
        if(!$ssh->login($this->project_config["connection"]["username"], $this->project_config["connection"]["password"])) {
            throw new Error("Could not login to SSH. Now exiting");
            exit;
        }

        $ssh->exec('cd '.$this->project_config["connection"]["server_path"].'; mysqldump --host=localhost --user='.$this->project_config["sql"]["username"].' --password='.$this->project_config["sql"]["password"].' '.$this->project_config["sql"]["database"].' > dump.sql');

        $sftp = new \phpseclib3\Net\SFTP($this->project_config["connection"]["host"], $this->project_config["connection"]["port"]);
        if(!$sftp->login($this->project_config["connection"]["username"], $this->project_config["connection"]["password"])) {
            throw new Error("Could not login with SFTP. Now exiting");
            exit;
        }
        $sftp->chdir($this->project_config["connection"]["server_path"]);
        $sftp->get("dump.sql", "$this->projects_dir\\$this->project_name\dump.sql");
        $ssh->exec('cd '.$this->project_config["connection"]["server_path"].'; rm dump.sql');

        $sftp->disconnect();
        $ssh->disconnect();
    }

    private function downloadSqlDump() {
        $this->deleteDir(PROJECTS_DIR."\\$this->project_name\mysql");
        $sql = $this->project_config["sql"];
        exec('mysqldump -h '.$sql["host"].' -u '.$sql["username"].' -p'.$sql["password"].' '.$sql["database"].' --no-tablespaces > '.PROJECTS_DIR.'\\'.$this->project_name.'\dump.sql');
    }

    private function downloadProjectFromFtp() {
        $config = $this->project_config["connection"];
        $ftpConnection = ftp_connect($config["host"], $config["port"], 10);
        ftp_login($ftpConnection, $config["username"], $config["password"]);
        ftp_pasv($ftpConnection, true);
        $this->ftp_download_all_files($ftpConnection, "{$config["server_path"]}/wp-content", PROJECTS_DIR."\\$this->project_name\site");
        ftp_close($ftpConnection);
    }

    private function downloadProjectFromKinsta() {
        $sftp = new \phpseclib3\Net\SFTP($this->project_config["connection"]["host"], $this->project_config["connection"]["port"]);
        if(!$sftp->login($this->project_config["connection"]["username"], $this->project_config["connection"]["password"])) {
            throw new Error("Could not login to SFPT. Now exiting");
            exit;
        }
        $this->sftp_download_all_files($sftp, $this->project_config["connection"]["server_path"].'/wp-content', PROJECTS_DIR."\\$this->project_name\site");
        $sftp->disconnect();
    }

    private function sftp_download_all_files($connection, $_from = null, $_to = null, $current_dir = null) {
        if (isset($_from)) {
            $connection->chdir($_from);
            if (isset($_to)) {
                if (!is_dir($_to)) @mkdir($_to);
                if (!chdir($_to)) die("Dir on local not exists? $_to");
            }
        }

        $contents = $connection->rawlist();

        foreach ($contents as $file => $p) {

            Helper::log($file);
            if($file == "." || $file == "..") continue;
            if ($p['type'] != 2 && $p['type'] != 1) continue;

            Helper::log($file);

            Helper::log($connection->pwd().'/'.$file);

            if (file_exists($file) && !is_dir($file) && filemtime($file) >= strtotime($p['mtime'])) {
                Helper::log("File already exists and is not modified, skipping...");
            }
            elseif ($p['type'] == 1) {
                Helper::log("IS FILE");
                $connection->get($file, $file);
                Helper::log("Downloaded...");
            }
            elseif ($p['type'] == 2) {
                Helper::log("IS DIR");
                if($file === "minit" && $current_dir === "uploads") {
                    Helper::log("Skipping minit folder...");
                    continue;
                }
                if (strpos($file, 'twentytwenty') === 0) {
                    Helper::log("Skipping twentytwenty theme...");
                    continue;
                }
                if ($file === "advancedassets_images") {
                    Helper::log("Skipping advanced assets generated images...");
                    continue;
                }
                if($file === "themes") {
                    Helper::log("Skipping themes folder...");
                    continue;
                }
                Helper::log("Dir changed to $file");
                if (!is_dir($file)) mkdir($file);
                chdir($file);
                $connection->chdir('./'.$file);
                $this->sftp_download_all_files($connection, null, null, $file);
                $connection->chdir("..");
                chdir('..');
            }
        }
    }

    /**
     * Starts a new project
     * A new project is a project that has not been run on this machine before.
     * @return array
     */

    private function createNewProject() {
        Helper::log("Setting up remote settings:");
        Helper::log("When this is done, this will be saved and remembered for the next time you want to boot the project again.", 2);

        $project_config                                 = [];
        $project_config["connection"]["type"]           = Helper::user_input("Connection type [ftp/kinsta]: ", ["ftp", "kinsta"]);
        $project_config["connection"]["host"]           = Helper::user_input(strtoupper($project_config["connection"]["type"])." hostname / IP address: ");
        $project_config["connection"]["username"]       = Helper::user_input("Login / username: ");
        $project_config["connection"]["password"]       = Helper::user_input("Password: ");
        if($project_config["connection"]["type"] === "kinsta") {
            $project_config["connection"]["port"]           = Helper::user_input("Port defined in kinsta dashboard: ");
        } else {
            $project_config["connection"]["port"]           = Helper::user_input("Port (leave empty for default: 21): ");
        }
        $project_config["connection"]["server_path"]    = Helper::user_input("Server path (etc.: /var/www/public): ");

        if(empty($project_config["connection"]["port"])) {
            $project_config["connection"]["port"] = 21;
        }

        Helper::log($project_config["connection"]);
        $confirmation                                   = strtolower(Helper::user_input("Please confirm the above [y/n]", ["y", "n"]));


        if($confirmation === "n") {
            $this->createNewProject();
        } else {

            Helper::log("Testing FTP/SSH credentials... please wait");
            $this->testConnection($project_config["connection"]);


            if($project_config["connection"]["type"] === "ftp") {
                $project_config["sql"]["host"] = Helper::user_input("SQL hostname / IP address: ");
            }
            $project_config["sql"]["username"] = Helper::user_input("SQL login / username: ");
            $project_config["sql"]["password"] = Helper::user_input("SQL password: ");
            if($project_config["connection"]["type"] === "ftp") {
                $project_config["sql"]["port"] = Helper::user_input("SQL port (leave empty for default: 3306): ");
                if (empty($project_config["sql"]["port"])) {
                    $project_config["sql"]["port"] = 3306;
                }
            }

            if($project_config["connection"]["type"] === "kinsta") {
                $project_config["sql"]["database"] = Helper::user_input("Database name: ");
                $project_config["sql"]["prefix"] = "wp";
            }


            Helper::log($project_config["connection"]);
            Helper::log($project_config["sql"]);
            $confirmation = strtolower(Helper::user_input("Please confirm the above [y/n]", ["y", "n"]));

            if ($confirmation === "n") {
                $this->createNewProject();
            } else {
                Helper::log("Testing SQL credentials... please wait");
                if($project_config["connection"]["type"] === "ftp") {
                    $databaseSelectors = $this->testSQL($project_config["sql"]);
                    $project_config["sql"]["database"] = $databaseSelectors["name"];
                    $project_config["sql"]["prefix"] = $databaseSelectors["prefix"];
                }
            }
            $encrypted = openssl_encrypt(json_encode($project_config), 'AES-192-CBC', $this->key);
            file_put_contents("$this->projects_dir\\$this->project_name\config.key", $encrypted);
            return $project_config;
        }
    }

    private function testSQL($config) {
        $databases = null;
        $result = null;
        exec('mysql -u '.$config["username"].' -p'.$config["password"].' -h '.$config["host"].' --execute="show databases;"', $databases, $result);
        if(!$databases || empty($databases) || $result === 1) {
            throw new Error("SQL seems broken, cannot fetch new site. Now exiting.");
            exit;
        }


        $databases = array_values(array_diff( $databases, ["Database", "information_schema"] ));
        if(count($databases) === 1) {
            $actualDatabase = $databases[0];
        } else {
            foreach($databases as $key => $database) {
                Helper::log("[$key] = $database");
            }
            $selectedDatabase = Helper::user_input("Please select a database by number: ", array_keys($databases));
            $actualDatabase = $databases[$selectedDatabase];
        }
        Helper::log("You have selected the database: $actualDatabase", 2);

        $tables = null;
        exec('mysql -u '.$config["username"].' -p'.$config["password"].' -h '.$config["host"].' --execute="use '.$actualDatabase.'; show tables;"', $tables, $tablesResult);
        if(!$tables || empty($tables) || $tablesResult === 1) {
            throw new Error("SQL seems broken, cannot fetch new site. Now exiting.");
            exit;
        }
        if(count($tables) === 1) {
            $actualTable = explode("_", $tables[0])[0];
        } else {
            $uniquePrefixes = [];
            foreach($tables as $key => $table) {
                $prefix = explode("_", $table)[0];
                if(!in_array($prefix, $uniquePrefixes)) {
                    $uniquePrefixes[] = $prefix;
                }
            }
            foreach($uniquePrefixes as $key => $uniquePrefix) {
                Helper::log("[$key] = $uniquePrefix");
            }
            $selectedTable = Helper::user_input("Please select a prefix by number: ", array_keys($uniquePrefixes));
            $actualTable = $uniquePrefixes[$selectedTable];
        }
        Helper::log("You have selected the prefix: $actualTable", 2);


        return [
            "name"      => $actualDatabase,
            "prefix"    => $actualTable
        ];
    }

    /**
     * Test a configuration by connecting with the config and testing for certain patterns to determine if everything is o.k.
     * Test will disconnect as soon as everything is checked or if anything is incorrect.
     * @param $connection_config
     */
    private function testConnection($connection_config = null) {
        if(!$connection_config) {
            $connection_config = $this->project_config["connection"];
        }

        if(!is_array($connection_config)) {
            $reset = Helper::user_input("Connection configuration seems to be broken, want to reset? [y/n]: ", ["y", "n", "yes", "no"]);
            if($reset === "n" || $reset === "no") {
                throw new Error("Connection configuration is broken, cannot build. Now exiting...");
            }
        }

        $required_parameters = [
            "type",
            "host",
            "username",
            "password",
            "port",
            "server_path"
        ];

        if(array_diff_key(array_flip($required_parameters), $connection_config)) {
            $reset = Helper::user_input("The connection config is missing some configuration, want to reset the config?", ["y", "n", "yes", "no"]);
            if($reset === "n" || $reset === "no") {
                throw new Error("Connection configuration is broken, cannot build. Now exiting...");
            }

            $config = $this->createNewProject();
            $this->testConnection($config);
        }

        if($connection_config["type"] === "ftp") {
            if(!$this->testFtpConfig($connection_config)) {
                throw new Error("Connection configuration seems to be broken, cannot connect to remote host.");
            }
        }
        if($connection_config["type"] === "kinsta") {
            $this->testSSHConfig($connection_config);
        }
    }

    private function testFtpConfig($config) {
        $testConnection     = ftp_connect($config["host"], $config["port"], 10);
        $ftp                = ftp_login($testConnection, $config["username"], $config["password"]);

        if((!$ftp) || (!$testConnection)) {
            Helper::log("ERROR: Could not connect to {$config["host"]}:{$config["port"]} with login {$config["username"]}");
            ftp_close($testConnection);
            return false;
        }
        Helper::log("Connection to server was successful!");

        if(!ftp_chdir($testConnection, $config["server_path"])) {
            Helper::log("ERROR: Could not change to server path: {$config["server_path"]}");
            ftp_close($testConnection);
            return false;
        }
        Helper::log("Successfully changed to directory: {$config["server_path"]}");

        if(!ftp_nlist($testConnection, "wp-content")) {
            Helper::log("ERROR: Could not find wp-content folder inside: {$config["server_path"]}");
            ftp_close($testConnection);
            return false;
        }
        Helper::log("Found wp-content folder inside {$config["server_path"]}");

        Helper::log("Configuration tested with success...", 2);
        ftp_close($testConnection);
        return true;
    }

    private function testSSHConfig($config) {
        $ssh = new \phpseclib3\Net\SSH2($config["host"], $config["port"]);
        if(!$ssh->login($config["username"], $config["password"])) {
            throw new Error("Could not login to SSH. Now exiting");
            exit;
        }
        $ssh->disconnect();
    }
}

new Pull();