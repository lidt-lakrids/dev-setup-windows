# dev-setup-windows

Bootstrap setup for developers working at Lidt Lakrids (WINDOWS / POWERSHELL)


## TLDR;

To install, run:
`powershell "IEX(New-Object Net.WebClient).downloadString('https://gitlab.com/lidt-lakrids/dev-setup-windows/-/raw/main/dev-setup-windows.ps1')"`

## Step by step

### #1 Run powershell
Smack the windows button and type "powershell" to search for the powershell cli.
Open the powershell cli.

### #2 Copy this string
`powershell "IEX(New-Object Net.WebClient).downloadString('https://gitlab.com/lidt-lakrids/dev-setup-windows/-/raw/main/dev-setup-windows.ps1')"`

### #3 Insert the string into your powershell
CTRL+V or right click inside the powershell, and paste the string.

### #4 Run
Hit "Enter"-key to run the powershell script.

## What does this do?
This command will run the .ps1 (powershell script) located on the URL defined in the string.
The URL is a .ps1 file hosted on this GIT.

This script will handle installation of various software needed when working at Lidt Lakrids.

## What if i already have this software?

If you already have one or more of the software installed on your windows machine, it will automaticly be skipped.
If you have another version of some of the software, like PHP, this will install another PHP version on your machine.