<?php

class Build {
    public function __construct()
    {
        $this->build();
    }


    private function build()
    {
        if(!is_dir(CALLER_DIR."/wp-content/themes/lakrids")) {
            Helper::log("Cannot find 'lakrids' theme in ".CALLER_DIR."/wp-content/themes/lakrids");
            exit;
        }
        chdir(CALLER_DIR."/wp-content/themes/lakrids");
        passthru("npm run build");
    }


}

new Init();