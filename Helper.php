<?php

class Helper {


    /**
     * Displays the help output, which gives users a list of what commands is available for them to run
     */
    static function help() {
        $methodsAvailable   = [
            "help" => [
                "description"   => "Lists the available commands to run with the lakrids cli",
            ],
            "serve"             => [
                "description"   => "Serves a wordpress project from the current working directory ('-reset' to reset the connection config)",
            ]
        ];

        ob_start();
        self::log("------------------------------");
        self::log("Lakrids Help:");
        self::log("This CLI will take certain commands to run certain things. Find all the commands in this list:", 2);
        foreach($methodsAvailable as $method => $arguments) {
            self::log(strtoupper($method)."\t\t{$arguments['description']}");
        }
        self::log("------------------------------");
        ob_end_flush();
    }

    /**
     * Just a clean way of outputting messages back to the CLI
     * @param $message
     * @param int $lineBreaks
     */
    static function log($message, $lineBreaks = 1) {
        $breaks = "";
        $x = 0;
        while ($x < $lineBreaks) {
            $breaks .= "\n";
            $x++;
        }

        if(is_array($message)) {
            foreach($message as $m) {
                echo $m."\n";
            }
            echo $breaks;
        } else {
            echo $message . $breaks;
        }
    }

    static function deleteDir($dirPath) {
        if (! is_dir($dirPath)) {
            throw new InvalidArgumentException("$dirPath must be a directory");
        }
        if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
            $dirPath .= '/';
        }
        $files = glob($dirPath . '*', GLOB_MARK);
        foreach ($files as $file) {
            if (is_dir($file)) {
                self::deleteDir($file);
            } else {
                unlink($file);
            }
        }
        rmdir($dirPath);
    }

    /**
     * Request user for input.
     * You can specify a list of required answers for the user to choose from, if needed.
     * @param $info
     * @param null $requiredListOfAnswers
     * @return false|string
     */
    static function user_input($info, $requiredListOfAnswers = null) {
        $response = readline($info);
        if($requiredListOfAnswers) {
            if(!in_array($response, $requiredListOfAnswers)) {
                self::log("Wrong input. Please enter one of the following:");
                self::log(implode(" | ", $requiredListOfAnswers));
                return self::user_input($info, $requiredListOfAnswers);
            }
        }

        return $response;
    }


}