# show initial start message
Write-Host "# STARTING THE BOOTSTRAP PROCESS #" -ForegroundColor Yellow;
Write-Host "# Will install: #" -ForegroundColor Yellow;
Write-Host "- scoop.sh" -ForegroundColor Yellow;
Write-Host "- Git" -ForegroundColor Yellow;
Write-Host "- SSH" -ForegroundColor Yellow;
Write-Host "- Pshazz" -ForegroundColor Yellow;
Write-Host "- PHP" -ForegroundColor Yellow;
Write-Host "- PHP Composer" -ForegroundColor Yellow;
Write-Host "- NodeJS" -ForegroundColor Yellow;
Write-Host "- MariaDB" -ForegroundColor Yellow;
Write-Host "- JetBrains PHPStorm" -ForegroundColor Yellow;
Write-Host "- Docker-desktop" -ForegroundColor Yellow;
Write-Host "- Docker-compose" -ForegroundColor Yellow;

# remote signed to current user
Set-ExecutionPolicy RemoteSigned -scope CurrentUser;

# install scoop.sh
if (Get-Command scoop -errorAction SilentlyContinue)
{

} else {
    iwr -useb 'https://raw.githubusercontent.com/scoopinstaller/install/master/install.ps1' | iex;
}

# install
scoop install 7zip git;
scoop config SCOOP_REPO 'https://github.com/Ash258/Scoop-Core';
scoop update;
scoop status;
scoop checkup;
# Har ændret linkes

Get-ChildItem "$($home)\scoop\shims" -Filter 'scoop.*' | Copy-Item -Destination { Join-Path $_.Directory.FullName (($_.BaseName -replace 'scoop', 'shovel') + $_.Extension) };
shovel bucket add Base https://github.com/shovel-org/Base;
Write-Host "Finished installing scoop.sh... Continuing" -ForegroundColor Yellow;

# Updating scoop.sh
shovel update
Write-Host "Finished updating scoop.sh... Continuing" -ForegroundColor Yellow;

# install Git
shovel install git;
Write-Host "Finished installing Git... Continuing" -ForegroundColor Yellow;


shovel bucket add versions;
shovel install python27;

shovel install curl;

# install SSH
shovel install win32-openssh;
Write-Host "Finished installing SSH... Continuing" -ForegroundColor Yellow;

# install Putty
# scoop install putty;
# Write-Host "Finished installing Plink... Continuing" -ForegroundColor Yellow;

# install PHP
shovel install php;
Write-Host "Finished installing PHP... Continuing" -ForegroundColor Yellow;

# install PHP Composer
shovel install composer;
Write-Host "Finished installing PHP Composer... Continuing" -ForegroundColor Yellow;

# install NodeJS
shovel install nodejs;
Write-Host "Finished installing NodeJS... Continuing" -ForegroundColor Yellow;

# shovel install python27;

# install Vue-CLI
# npm install -g @vue/cli;
# npm install -g npm-run-all;
# npm install --global --production windows-build-tools --vs2015;
# npm install -g windows-build-tools;
# npm config set msvs_version 2015 --global;
npm install --global gulp-cli;
Write-Host "GULP CLI... Continuing" -ForegroundColor Yellow;

# install mariaDB
shovel install mariadb;
Write-Host "Finished installing MariaDB... Continuing" -ForegroundColor Yellow;

# install PHPStorm
shovel bucket add jetbrains;
shovel install PhpStorm;
Write-Host "Finished installing PHPStorm... Continuing" -ForegroundColor Yellow;

# install docker
# scoop install docker;
# Write-Host "Finished installing Docker... Continuing" -ForegroundColor Green;
shovel install docker-compose;
Write-Host "Finished installing Docker-compose... Continuing" -ForegroundColor Yellow;

# install docker desktop
shovel install 7zip git;
shovel config SCOOP_REPO 'https://github.com/Ash258/Scoop-Core'
shovel bucket add Ash258 'https://github.com/Ash258/Scoop-Ash258.git';
shovel bucket add extras;
shovel install docker;
Write-Host "Finished installing Docker-desktop... Continuing" -ForegroundColor Yellow;



# installing Lidt Lakrids PHP script as global cli
Write-Host "Installing Lidt Lakrids PHP building CLI script.... Please wait" -ForegroundColor Yellow;

# setup PHP modules needed within scoop.sh
$scoopPHPConfdDir = "$($home)\scoop\persist\php\cli\conf.d";
Set-Content "$($scoopPHPConfdDir)\extensions.ini" "extension_dir=ext" | Out-Null;
Add-Content -Path "$($scoopPHPConfdDir)\extensions.ini" -Value "extension=ftp";
Add-Content -Path "$($scoopPHPConfdDir)\extensions.ini" -Value "extension=openssl";
Add-Content -Path "$($scoopPHPConfdDir)\extensions.ini" -Value "extension=fileinfo";
Add-Content -Path "$($scoopPHPConfdDir)\extensions.ini" -Value "extension=gd";

Set-Content "$($scoopPHPConfdDir)\custom.ini" "max_execution_time = 60" | Out-Null;
Add-Content -Path "$($scoopPHPConfdDir)\custom.ini" -Value "memory_limit = 256M";
Add-Content -Path "$($scoopPHPConfdDir)\custom.ini" -Value "post_max_size = 128M";
Add-Content -Path "$($scoopPHPConfdDir)\custom.ini" -Value "upload_max_filesize = 128M";



# create custom folder for lidt lakrids cli
$folderName = "lakrids_cli";
$drive      = "$($pwd.drive.name):";
$path       = "$($drive)\$($folderName)";
if(!(test-path $path))
{
    New-Item -Path "$($drive)" -Name "$($folderName)" -ItemType "directory" | Out-Null;
}

# inject the uploads.ini into the project
$webClient = New-Object System.Net.WebClient
New-Item -Path "$($path)" -Name "uploads.ini" -ItemType "file" -Value $webClient.DownloadString('https://gitlab.com/lidt-lakrids/dev-setup-windows/-/raw/main/uploads.ini') -Force | Out-Null;

# create the PHP injected script
$webClient = New-Object System.Net.WebClient
New-Item -Path "$($path)" -Name "run_cmd.php" -ItemType "file" -Value $webClient.DownloadString('https://gitlab.com/lidt-lakrids/dev-setup-windows/-/raw/main/run_cmd.php') -Force | Out-Null;
New-Item -Path "$($path)" -Name "Helper.php" -ItemType "file" -Value $webClient.DownloadString('https://gitlab.com/lidt-lakrids/dev-setup-windows/-/raw/main/Helper.php') -Force | Out-Null;
New-Item -Path "$($path)" -Name "serve.php" -ItemType "file" -Value $webClient.DownloadString('https://gitlab.com/lidt-lakrids/dev-setup-windows/-/raw/main/serve.php') -Force | Out-Null;
New-Item -Path "$($path)" -Name "remove.php" -ItemType "file" -Value $webClient.DownloadString('https://gitlab.com/lidt-lakrids/dev-setup-windows/-/raw/main/remove.php') -Force | Out-Null;
New-Item -Path "$($path)" -Name "init.php" -ItemType "file" -Value $webClient.DownloadString('https://gitlab.com/lidt-lakrids/dev-setup-windows/-/raw/main/init.php') -Force | Out-Null;

# create the bat file to execute PHP script
Set-Content "$($path)\lakrids.bat" "@echo OFF" | Out-Null;
$dockerOutput = "FOR /f ""tokens=*"" %%i IN ('docker ps -q') DO docker stop %%i" -f $path;
$output = 'php {0}\run_cmd.php "%cd%" %*' -f $path;
Add-Content -Path "$($path)\lakrids.bat" -Value $dockerOutput;
Add-Content -Path "$($path)\lakrids.bat" -Value $output;

# update environment PATH to include our CLI
$userEnvPaths = [System.Environment]::GetEnvironmentVariable('Path','User');
if (!($userEnvPaths | Select-String -SimpleMatch $path))
{
    $new  =  "$($userEnvPaths);$($path)";
    [System.Environment]::SetEnvironmentVariable('Path', $new, 'User');
}

# finished
wsl --set-default-version 2;
Write-Host "PLEASE RESTART THE COMPUTER TO FINISH INSTALLATION" -ForegroundColor Green;
Write-Host "To use the build script, use 'lakrids build' inside the PHPStorm terminal, when inside a wordpress project" -ForegroundColor Yellow;
Write-Host "Thank you for joining the Lidt Lakrids team!" -ForegroundColor Yellow;
restart-computer -Confirm;